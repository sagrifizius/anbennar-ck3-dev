k_gawed = {
	800.1.1 = {
		succession_laws = { male_only_law }
	}
	923.12.27 = {
		holder = 526 #Carlan Gawe inherits from his father
	}
	950.4.7 = {
		holder = 525 #Ulric Gawe
	}
	956.12.4 = {
		holder = 523 #Godrac Gawe
	}
	986.5.23 = {
		holder = 51 #Alenn Gawe
	}
}

d_westmounts = {
	1018.4.19 = {
		holder = 98 #Toman Wight
		liege = "k_gawed"
	}
}

k_eaglecrest = {
	1010.4.2 = {
		holder = 56 #Warde Eaglecrest
	}
}

c_vanbury = {
	800.1.1 = {
		liege = "k_gawed"
	}
	923.12.27 = {
		holder = 526 #Carlan Gawe inherits from his father
	}
	949.3.28 = {
		holder = 527 #Ricard of Vanbury gifted by his father for his talents
	}
	982.5.29 = {
		holder = 528 #Carlan of Vanbury
	}
	1015.4.10 = {
		holder = 529 #Godryc of Vanbury
	}
}

d_beronmoor = {
	990.4.6 = {
		holder = 57 #Garther Beron
	}
}

d_moorhills = {
	1008.7.4 = {
		holder = 58 #Deris Fouler
	}
}

d_westmoor = {
	1003.3.2 = {
		holder = 59 #Marlen Cottersea
	}
}

k_arbaran = {
	1015.9.8 = {
		holder = 60 #Ianren the Rider
	}
}

d_arbaran = {
	1015.9.8 = {
		holder = 60 #Ianren the Rider
	}
}

d_crodam = {
	1015.9.8 = {
		holder = 60 #Ianren the Rider
	}
}
