k_cast = {
	1020.11.5 = {
		holder = 89 #Wynstan sil Cast
	}
}

k_anor = {
	1020.11.5 = {
		holder = 90 #Lain sil Anor
	}
}

d_foarhal = {
	1020.11.10 = {
		liege = "k_anor"
		holder = 514 #Iacob the Betrayer
	}
}

d_bradsecker = {
	1020.11.10 = {
		liege = "k_anor"
		holder = 514 #Iacob the Betrayer
	}
}

d_steelhyl = {
	1021.10.31 = {		#Grand Ball of Anbenncost + Treaty of Anbenncost
		liege = "k_anor"
		holder = 530 #Caylen of Vanbury-Steelhyl, given to him to revitialize Escanni economy + pay dividends to Free Realms
	}
}

k_castonath = {
	1020.10.31 = {
		holder = 92 #Chlothar of Castonath
		government = republic_government
	}
}