﻿try_to_add_racial_purist_effect = {
	if = {
		limit = { NOT = { has_variable = racial_attitude_set } }
		add_trait = elven_purist
		add_trait = human_purist
		add_trait = gnomish_purist
		add_trait = dwarven_purist
		add_trait = half_elf_purist
		add_trait = halfling_purist
	}
}
