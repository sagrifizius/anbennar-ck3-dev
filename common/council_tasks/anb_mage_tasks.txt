﻿task_court_mage_default = {
	position = councillor_mage
	default_task = yes
	
	task_type = task_type_general
	task_progress = task_progress_infinite
	
	effect_desc = task_advisor_default_effect_desc
	council_owner_modifier = {
		name = task_advisor_default_modifier
	}
}