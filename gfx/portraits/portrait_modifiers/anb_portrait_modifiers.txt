﻿
anb_racial_appearance = {
	usage = game

	elf = {
		dna_modifiers = {
			morph = {
				mode = replace				
				gene = gene_age
				template = anb_elf_aging
				value = 1.0
			}
		}
		weight = {
			base = 0
			modifier = {
				add = 100
				has_trait = elf
			}
		}
	}

}